import json
from datetime import datetime

import requests
from django.contrib import messages
from django.contrib.auth import authenticate, login as auth_login, login as Login, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render, render_to_response
from django.template.context_processors import csrf
from rest_framework.authtoken.models import Token

from .forms import (RegistrationForm,
                    UserForm,
                    ProfileForm,
                    ChangePasswordForm,
                    WebsiteEditForm,
                    WebsiteAddForm
                    )
from .models import UserInfo, Website
# Create your views here.
from .utils import get_name_server, get_ip_address, get_website_object


def home(request):
    return render(request, 'pages/home.html')


def login(request):
    if request.method == "POST":
        email = request.POST.get('email').strip()
        password = request.POST.get('password').strip()

        if '@' in email:
            username = get_object_or_404(User, email=email)
            user = authenticate(username=username, password=password)
            if user is not None:
                auth_login(request, user)
                return HttpResponseRedirect('/')
            else:
                messages.error(request, "Tài khoản hoặc mật khẩu không đúng")
                return render(request, 'pages/login.html')
        else:
            user = authenticate(username=email, password=password)
            if user is not None:
                auth_login(request, user)
                return HttpResponseRedirect('/')
            else:
                messages.error(request, "Tài khoản hoặc mật khẩu không đúng")
                return render(request, 'pages/login.html')
    return render(request, 'pages/login.html')


def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = form.save()
            Login(request, user, backend='django.contrib.auth.backends.ModelBackend')
            return HttpResponseRedirect('/')
        else:
            return render(request, 'pages/register.html', {"form": form})
    else:
        form = RegistrationForm()
        return render(request, 'pages/register.html', {'form': form})


def landing_page(request):
    return render(request, 'pages/landing_page.html')


@login_required(login_url="login")
def profile(request, user_id):
    user = get_object_or_404(User, id=user_id)
    if request.method == "POST":
        user_info = UserInfo.objects.get(user=user)
        user_form = UserForm(data=request.POST or None, instance=user)
        profile_form = ProfileForm(data=request.POST or None, instance=user_info)

        user = user_form.save(commit=True)
        user.save()
        if profile_form.is_valid():
            user_info = profile_form.save(commit=False)
            user_info.save()
            messages.success(request, "Cập nhật thành công!")
            context = {'user': user_form,
                       'user_info': profile_form,
                       'user_id': user_id
                       }
            return render(request, 'pages/profile.html', context=context)
        else:
            messages.error(request, "Cập nhật không thành công! Kiểm tra lại thông tin")
            context = {'user': user_form,
                       'user_info': profile_form,
                       'user_id': user_id
                       }
            return render(request, 'pages/profile.html', context=context)
    else:
        if UserInfo.objects.filter(user=user).exists():
            user_info = UserInfo.objects.get(user=user)
        else:
            user_info = UserInfo.objects.create(user_id=user.id)
        user_form = UserForm(instance=user)
        profile_form = ProfileForm(instance=user_info)
        context = {'user': user_form,
                   'user_info': profile_form,
                   'user_id': user_id
                   }
        return render(request, 'pages/profile.html', context=context)


@login_required(login_url="login")
def change_password(request):
    if request.method == "POST":
        form = ChangePasswordForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            messages.success(request, "Mật khẩu được thay đổi thành công")
            return redirect('change_password')
    else:
        form = ChangePasswordForm(request.user)
    return render(request, 'pages/change_password.html', {'form': form})


@login_required(login_url="login")
def retrieve_websites(req):
    url_api = 'http://' + str(req.META['HTTP_HOST']) + '/api/v1/website'
    token, _ = Token.objects.get_or_create(user=req.user)
    headers = {'Authorization': 'Token ' + token.key}
    result = requests.get(url=url_api, headers=headers)

    args = {}
    args.update(csrf(req))
    args['contents'] = result.json()
    return render(req, 'websites/websites.html', args)


@login_required(login_url="login")
def add_website(req):
    if req.method == 'POST':
        form = WebsiteAddForm(req.POST, initial={'user_id': req.user})
        url_api = 'http://' + str(req.META['HTTP_HOST']) + '/api/v1/website'
        token, _ = Token.objects.get_or_create(user=req.user)
        headers = {'Authorization': 'Token ' + token.key}
        if form.is_valid():
            data = {
                'name': form.data.get('name'),
                'uri': form.data.get('uri'),
                'status_monitor': form.data.get('status_monitor'),
                'gaptime_monitor': form.data.get('gaptime_monitor'),
                'user_id': req.user.id,
                'name_server': get_name_server(domain=form.data.get('uri')),
                'ip_address': get_ip_address(domain=form.data.get('uri'))
            }
            result = requests.post(url=url_api, data=data, headers=headers)
            if result.status_code == 201:
                messages.success(req, "Website được tạo thành công")
                return HttpResponseRedirect('/websites')
            else:
                messages.error(req, "Có lỗi xảy ra! Vui lòng kiểm tra lại")
                render_to_response('websites/add_website.html')
        else:
            return render_to_response('websites/add_website.html', {'form': form})
    else:
        form = WebsiteAddForm(initial={'user_id': req.user})
    context = {
        'form': form
    }
    return render(req, 'websites/add_website.html', context)


@login_required(login_url="login")
def edit_website(req, id):
    url_api = "http://" + str(req.META['HTTP_HOST']) + '/api/v1/website/'
    token, _ = Token.objects.get_or_create(user=req.user)
    headers = {'Authorization': 'Token ' + token.key}
    result = requests.get(url=url_api + str(id), headers=headers)

    web = Website()
    web = get_website_object(website_model=web, data=result.json(), user=req.user)

    if req.method == 'POST':
        form = WebsiteEditForm(req.POST, instance=web)
        print(form.errors.as_data())
        if form.is_valid():
            data = {
                'name': form.data.get('name'),
                'uri': form.data.get('uri'),
                'status_monitor': form.data.get('status_monitor'),
                'gaptime_monitor': form.data.get('gaptime_monitor'),
                'user_id': req.user.id,
                'name_server': get_name_server(domain=form.data.get('uri')),
                'ip_address': get_ip_address(domain=form.data.get('uri'))
            }
            result = requests.put(url=url_api + str(id), data=data, headers=headers)
            if result.status_code == 204:
                messages.success(req, "Cập nhật trang web theo dõi thành công")
                return HttpResponseRedirect('/websites/' + str(id))

    form = WebsiteEditForm(instance=web)
    context = {
        'form': form,
        'website': web
    }
    return render(req, 'websites/edit_website.html', context)


@login_required(login_url="login")
def delete_website(req, id):
    url_api = 'http://' + str(req.META['HTTP_HOST']) + '/api/v1/website/' + str(id)
    token, _ = Token.objects.get_or_create(user=req.user)
    headers = {'Authorization': 'Token ' + token.key}
    result = requests.delete(url=url_api, headers=headers)

    if result.status_code == 204:
        return HttpResponseRedirect('/websites/')


@login_required(login_url='login')
def monitor_website(req, id):
    url_api = "http://" + str(req.META['HTTP_HOST']) + '/api/v1/website/' + str(id)
    token, _ = Token.objects.get_or_create(user=req.user)
    headers = {'Authorization': 'Token ' + token.key}

    result = requests.get(url=url_api, headers=headers)
    if result.status_code == 200:
        web = Website()
        web = get_website_object(website_model=web, data=result.json(), user=req.user)

        website_name_servers = json.loads(web.name_server)
        website_name_servers_string = (",").join(website_name_servers)

        context = {
            'website': web,
            'website_name_servers': website_name_servers_string
        }
        return render(req, 'websites/monitor_website.html', context)
    return HttpResponseRedirect('/websites/')
