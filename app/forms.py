import json
import socket

import dns
from django import forms
from django.contrib.auth import password_validation
from django.contrib.auth.forms import UserCreationForm, PasswordResetForm, PasswordChangeForm
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.forms import ModelForm

from app.models import UserInfo, Website


class RegistrationForm(UserCreationForm):
    first_name = forms.CharField(required=True, widget=forms.TextInput(attrs={
        'type': 'text',
        'placeholder': 'Họ',
        'class': 'form-control'}))
    last_name = forms.CharField(required=True, widget=forms.TextInput(attrs={
        'type': 'text',
        'placeholder': 'Tên',
        'class': 'form-control'}))
    email = forms.EmailField(required=True, widget=forms.EmailInput(attrs={
        'type': 'text',
        'placeholder': 'Email',
        'class': 'form-control'}))
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={
        'type': 'password',
        'placeholder': 'Mật khẩu',
        'class': 'form-control'}))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={
        'type': 'password',
        'placeholder': 'Nhập lại mật khẩu',
        'class': 'form-control'}))

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'password1', 'password2')

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if User.objects.filter(email=email).exists():
            raise ValidationError(
                "Email đã tồn tại"
            )
        return email

    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        if commit:
            user.username = self.cleaned_data['email']
            user.save()
        return user


class EmailValidationOnForgotPassword(PasswordResetForm):
    def clean_email(self):
        email = self.cleaned_data['email']
        if not User.objects.filter(email__iexact=email, is_active=True).exists():
            raise ValidationError(
                "Không tìm thấy địa chỉ email."
            )
        return email


class UserForm(ModelForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email']
        widgets = {
            'first_name': forms.TextInput(attrs={
                'type': 'text',
                'placeholder': 'Họ',
                'class': 'form-control'
            }),
            'last_name': forms.TextInput(attrs={
                'type': 'text',
                'placeholder': 'Tên',
                'class': 'form-control'
            }),
            'email': forms.EmailInput(attrs={
                'placeholder': 'Email',
                'class': 'form-control'
            }),
        }
        labels = {
            'first_name': "Họ",
            'last_name': "Tên",
            'email': "Email",
        }


class ProfileForm(ModelForm):
    class Meta:
        model = UserInfo
        fields = ['date_of_birth', 'gender', 'address', 'phone_number']
        widgets = {
            'date_of_birth': forms.DateInput(attrs={
                'placeholder': 'YYYY-MM-DD',
                'class': 'form-control',
                'type': 'text',
            }),
            'address': forms.TextInput(attrs={
                'type': 'text',
                'placeholder': 'Địa chỉ',
                'class': 'form-control'
            }),
            'phone_number': forms.TextInput(attrs={
                'type': 'text',
                'placeholder': 'Số điện thoại',
                'class': 'form-control'
            })
        }
        labels = {
            'date_of_birth': "Ngày sinh",
            'gender': "Giới tính",
            'address': "Địa chỉ",
            'phone_number': "Số điện thoại",
        }


class ChangePasswordForm(PasswordChangeForm):
    old_password = forms.CharField(required=True, widget=forms.PasswordInput(attrs={
        'type': "password",
        'class': 'form-control',
        'placeholder': "Mật khẩu cũ"
    }))
    new_password1 = forms.CharField(required=True, widget=forms.PasswordInput(attrs={
        'type': 'password',
        'class': 'form-control',
        'placeholder': 'Mật khẩu mới',
        'onkeyup': "checkPasswordLength('id_new_password1', 'new_pass_mess_1');"
    }))
    new_password2 = forms.CharField(required=True, widget=forms.PasswordInput(attrs={
        'type': 'password',
        'class': 'form-control',
        'placeholder': 'Nhập lại mật khẩu mới',
        'onkeyup': "matchingPassword('id_new_password2', 'new_pass_mess_2');"
    }))

    def clean_old_password(self):
        old_password = self.cleaned_data['old_password']
        if not self.user.check_password(old_password):
            raise forms.ValidationError(
                "Mật khẩu cũ không đúng"
            )
        return old_password

    def clean_new_password2(self):
        password1 = self.cleaned_data['new_password1']
        password2 = self.cleaned_data['new_password2']

        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                'Hai mật khẩu không khớp'
            )
        password_validation.validate_password(
            self.cleaned_data['new_password2']
        )
        return password2


class WebsiteAddForm(forms.ModelForm):
    CHOICES = [
        (300, '5 phút'), (600, '10 phút'), (1800, '30 phút'),
        (3600, '1 giờ'), (21600, '6 giờ'), (43200, '12 giờ'), (86400, '24 giờ')
    ]
    name = forms.CharField(required=True, label="Tên trang web", widget=forms.TextInput(attrs={
        'type': 'text',
        'placeholder': 'Tên trang web',
        'class': 'form-control'
    }))
    uri = forms.CharField(required=True, label="URL trang web mà bạn muốn theo dõi", widget=forms.TextInput(attrs={
        'type': 'text',
        'placeholder': 'https://example.com',
        'class': 'form-control'
    }))
    gaptime_monitor = forms.ChoiceField(choices=CHOICES, label="Tần suất kiểm tra")
    status_monitor = forms.ChoiceField(choices=[(True, 'Theo dõi'), (False, 'Tạm dừng')], label="Trạng thái")
    user_id = forms.HiddenInput()

    class Meta:
        model = Website
        fields = ['name', 'uri', 'status_monitor', 'gaptime_monitor']
        exclude = ['user_id']

    def clean_uri(self):
        uri = self.cleaned_data['uri']
        if Website.objects.filter(uri=uri).exists():
            raise forms.ValidationError('URL đã tồn tại')
        return uri


class WebsiteEditForm(forms.ModelForm):
    CHOICES = [
        (300, '5 phút'), (600, '10 phút'), (1800, '30 phút'),
        (3600, '1 giờ'), (21600, '6 giờ'), (43200, '12 giờ'), (86400, '24 giờ')
    ]
    name = forms.CharField(required=True, label="Tên trang web", widget=forms.TextInput(attrs={
        'type': 'text',
        'placeholder': 'Tên trang web',
        'class': 'form-control'
    }))
    uri = forms.CharField(required=True, label="URL trang web mà bạn muốn theo dõi", widget=forms.TextInput(attrs={
        'type': 'text',
        'placeholder': 'https://example.com',
        'class': 'form-control'
    }))
    gaptime_monitor = forms.ChoiceField(choices=CHOICES, label="Tần suất kiểm tra")
    status_monitor = forms.ChoiceField(choices=[(True, 'Theo dõi'), (False, 'Tạm dừng')], label="Trạng thái")
    user_id = forms.HiddenInput()

    class Meta:
        model = Website
        fields = ['name', 'uri', 'status_monitor', 'gaptime_monitor']
        exclude = ['user_id']
