from django.contrib.auth.models import User
from django.test import Client
from django.test import TestCase
from rest_framework import status
from rest_framework.authtoken.models import Token

from .api.serializers import WebsiteSerializer
from .forms import RegistrationForm, WebsiteAddForm
from .models import Website


# Create your tests here.


class RegistrationTest(TestCase):
    # create account for test
    def setUp(self):
        User.objects.create_user(
            'testcase@example.com', 'testcase@example.com', 'matkhau123')

    # Test: email address exists
    def test_registration_form_unique_email(self):
        form = RegistrationForm(data={
            'email': 'testcase@example.com',
            'first_name': 'Phạm',
            'last_name': 'Duy',
            'password1': 'testcase123',
            'password2': 'testcase123'
        })
        self.failIf(form.is_valid())
        self.assertEqual(form.errors['email'], [
            "Email đã tồn tại"])

    # Test: password is too similar to the email address
    def test_registration_form_smiilar_email(self):
        form = RegistrationForm(data={
            'email': 'testcase2@example.com',
            'first_name': 'Phạm',
            'last_name': 'Duy',
            'password1': 'testcase123',
            'password2': 'testcase123',
        })
        self.failIf(form.is_valid())
        self.assertEqual(form.errors['password2'], [
            "The password is too similar to the email address."])

    # Test: lenght password < 8
    def test_registration_form_too_short(self):
        form = RegistrationForm(data={
            'email': 'testcase2@example.com',
            'first_name': 'Phạm',
            'last_name': 'Duy',
            'password1': 'ithon8',
            'password2': 'ithon8',
        })
        self.failIf(form.is_valid())
        self.assertEqual(form.errors['password2'], [
            "This password is too short. It must contain at least 8 characters."])

    # Test: password is too common
    def test_registration_form_too_common(self):
        form = RegistrationForm(data={
            'email': 'testcase2@example.com',
            'first_name': 'Phạm',
            'last_name': 'Duy',
            'password1': 'password',
            'password2': 'password',
        })
        self.failIf(form.is_valid())
        self.assertEqual(form.errors['password2'], [
            "This password is too common."])

    # Test: valid email
    def test_registration_form_isValid_email(self):
        form = RegistrationForm(data={
            'email': 'test',
            'first_name': 'Phạm',
            'last_name': 'Duy',
            'password1': 'password123',
            'password2': 'password123',
        })
        self.failIf(form.is_valid())
        self.assertEqual(form.errors['email'], [
            "Enter a valid email address."])

    # Test: registration successfully
    def test_registration_form_valid(self):
        form = RegistrationForm(data={
            'email': 'duytien2401@gmail.com',
            'first_name': 'Phạm',
            'last_name': 'Duy',
            'password1': 'testcase123',
            'password2': 'testcase123'})
        self.assertTrue(form.is_valid())


class LoginTest(TestCase):
    # create account for test
    def setUp(self):
        User.objects.create_user(
            'testcase@example.com', 'testcase@example.com', 'matkhau123')

    # Test: login successfully
    def test_login_sucess(self):
        c = Client()
        res = c.login(username='testcase@example.com', password='matkhau123')
        self.assertTrue(res)

    # Test: login failed
    def test_login_fail(self):
        c = Client()
        res = c.login(username='testcase@example.com', password='matkhau321')
        self.assertFalse(res)


class LogoutTest(TestCase):
    # Test: logout
    def test_logout(self):
        c = Client()
        c.login(username='testcase@example.com', password='matkhau123')
        response = c.get('/logout/')
        self.assertEqual(response.status_code, 302)


class WebsiteTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(username='test', email='demo@example.com', password='testcase')
        self.website = Website.objects.create(
            name='facebook',
            uri='facebook.com',
            user_id=self.user
        )

    def test_form_is_valid(self):
        form = WebsiteAddForm(data={
            'name': 'google',
            'uri': 'google.com',
            'user_id': self.user,
            'status_monitor': 'True',
            'gaptime_monitor': '300',
            'ip_address': '31.13.77.35',
            'name_server': '["d.ns.facebook.com.", "c.ns.facebook.com.", "b.ns.facebook.com.", "a.ns.facebook.com."]'
        })
        self.assertTrue(form.is_valid())

    def test_uri_exist(self):
        form = WebsiteAddForm(data={
            'name': 'facebook',
            'uri': 'facebook.com',
            'user_id': self.user,
            'status_monitor': 'True',
            'gaptime_monitor': '300',
            'ip_address': '31.13.77.35',
            'name_server': '["d.ns.facebook.com.", "c.ns.facebook.com.", "b.ns.facebook.com.", "a.ns.facebook.com."]'
        })
        self.failIf(form.is_valid())
        self.assertEqual(form.errors['uri'], ['URL đã tồn tại'])

    def test_name_required_field(self):
        form = WebsiteAddForm(data={
            'uri': 'google.com',
            'user_id': self.user,
            'status_monitor': 'True',
            'gaptime_monitor': '300',
            'ip_address': '31.13.77.35',
            'name_server': '["d.ns.facebook.com.", "c.ns.facebook.com.", "b.ns.facebook.com.", "a.ns.facebook.com."]'
        })
        self.failIf(form.is_valid())
        self.assertEqual(form.errors['name'], ['This field is required.'])

    def test_uri_required_field(self):
        form = WebsiteAddForm(data={
            'name': 'demo',
            'user_id': self.user,
            'status_monitor': 'True',
            'gaptime_monitor': '300',
            'ip_address': '31.13.77.35',
            'name_server': '["d.ns.facebook.com.", "c.ns.facebook.com.", "b.ns.facebook.com.", "a.ns.facebook.com."]'
        })
        self.failIf(form.is_valid())
        self.assertEqual(form.errors['uri'], ['This field is required.'])

    def test_uri_not_valid(self):
        form = WebsiteAddForm(data={
            'name': 'facebook',
            'uri': 'facebook',
            'user_id': self.user,
            'status_monitor': 'True',
            'gaptime_monitor': '300',
            'ip_address': '31.13.77.35',
            'name_server': '["d.ns.facebook.com.", "c.ns.facebook.com.", "b.ns.facebook.com.", "a.ns.facebook.com."]'
        })
        self.failIf(form.is_valid())
        self.assertEqual(form.errors['uri'], ['URI không hợp lệ, hãy chắc chắn bạn đã nhập đúng'])
        self.assertEqual(form.errors['__all__'], ['Không tìm thấy địa chỉ IP'])

    def test_not_found_dns(self):
        form = WebsiteAddForm(data={
            'name': 'facebook',
            'uri': 'hus.vnu.edu.vn',
            'user_id': self.user,
            'status_monitor': 'True',
            'gaptime_monitor': '300',
            'ip_address': '31.13.77.35',
            'name_server': '["d.ns.facebook.com.", "c.ns.facebook.com.", "b.ns.facebook.com.", "a.ns.facebook.com."]'
        })
        self.failIf(form.is_valid())
        self.assertEqual(form.errors['__all__'], ['Không tìm thấy DNS Server'])


class WebsiteAPITest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(username='test', email='demo@example.com', password='testcase')
        self.website = Website.objects.create(
            id='dbec3b9787c343b49a1e86b9d695ce73',
            name='facebook',
            uri='facebook.com',
            user_id=self.user,
            name_server='["d.ns.facebook.com.", "c.ns.facebook.com.", "b.ns.facebook.com.", "a.ns.facebook.com."]',
            ip_address='31.13.77.35',
            gaptime_monitor='300',
            status_monitor='1'
        )
        self.token = Token.objects.create(user=self.user)

    def login(self):
        self.client.login(username='test', password='testcase')

    def test_get_all_website(self):
        self.login()
        response = self.client.get('http://' + '127.0.0.1' + '/api/v1/website')
        websites = Website.objects.all()
        serializer = WebsiteSerializer(websites, many=True)
        self.assertTrue(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.data, serializer.data)

    def test_get_website(self):
        self.login()
        response = self.client.get('http://' + '127.0.0.1' + '/api/v1/website/dbec3b9787c343b49a1e86b9d695ce73')
        website = Website.objects.get(id='dbec3b9787c343b49a1e86b9d695ce73')
        self.assertTrue(response.status_code, status.HTTP_200_OK)
