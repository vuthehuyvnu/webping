from rest_framework import viewsets
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.status import (
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_400_BAD_REQUEST,
    HTTP_204_NO_CONTENT,
)

from app.models import Website
from .serializers import WebsiteSerializer


class WebsiteViewSet(viewsets.ModelViewSet):
    queryset = Website.objects.all()
    serializer_class = WebsiteSerializer

    def get_queryset(self):
        queryset = Website.objects.all()
        return queryset

    def list(self, request, *args, **kwargs):
        queryset = Website.objects.filter(user_id=request.user)
        serializer = WebsiteSerializer(queryset, many=True)
        context = {
            'websites': serializer.data
        }
        return Response(context, status=HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        serializer = WebsiteSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=HTTP_201_CREATED)
        else:
            return Response(status=HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk=None, *args, **kwargs):
        website = self.get_object()
        website.delete()
        return Response(status=HTTP_204_NO_CONTENT)

    def update(self, request, *args, **kwargs):
        website = self.get_object()
        serializer = WebsiteSerializer(website, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(status=HTTP_204_NO_CONTENT)

    def retrieve(self, request, pk=None, *args, **kwargs):
        queryset = Website.objects.all()
        website = get_object_or_404(queryset, pk=pk)
        serializer = WebsiteSerializer(website)
        return Response(serializer.data, status=HTTP_200_OK)
