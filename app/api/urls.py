from django.urls import path

from . import views

website = views.WebsiteViewSet.as_view({
    'get': 'list',
    'post': 'create',
})
website_detail = views.WebsiteViewSet.as_view({
    'delete': 'destroy',
    'get': 'retrieve',
    'put': 'update'
})

urlpatterns = [
    path('api/v1/website', website),
    path('api/v1/website/<uuid:pk>', website_detail)
]
