import validators
from django.core.exceptions import ValidationError


def validate_uri(value):
    if validators.url(value) is not True and validators.domain(value) is not True:
        raise ValidationError("URI không hợp lệ, hãy chắc chắn bạn đã nhập đúng")
