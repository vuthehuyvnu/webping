import json
import socket
import uuid

import dns.resolver
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models

from app.validators import validate_uri


# Create your models here.


class UserInfo(models.Model):
    GENDERS = (
        ('nam', "Nam"),
        ('nữ', "Nữ"),
        ('khác', "Khác")
    )
    DATE_INPUT_FORMATS = ['%d-%m-%Y']
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    date_of_birth = models.DateField(null=True, blank=True)
    gender = models.CharField(choices=GENDERS, max_length=10, blank=True)
    address = models.CharField(max_length=255, null=True, blank=True)
    phone_number = models.CharField(max_length=10, null=True, blank=True)

    def __str__(self):
        return self.user.username


class Website(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=200, blank=False, default="Website monitor")
    uri = models.CharField(max_length=200, blank=False, validators=[validate_uri])
    user_id = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    status_monitor = models.BooleanField(default=True)
    gaptime_monitor = models.IntegerField(default=300)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)
    name_server = models.CharField(max_length=500, null=True)
    ip_address = models.CharField(max_length=500, null=True)

    def __str__(self):
        return self.name

    def clean(self):
        domain_name = self.uri
        try:
            self.ip_address = socket.gethostbyname(domain_name)
        except Exception:
            raise ValidationError("Không tìm thấy địa chỉ IP")

        try:
            self.name_server = json.dumps([str(x) for x in dns.resolver.query(domain_name, 'NS')])
        except Exception:
            raise ValidationError("Không tìm thấy DNS Server")
